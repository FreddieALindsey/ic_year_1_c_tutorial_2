#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include "main.h"

char *createCommand(size_t size) {
  /* Allocate space for one command */
  return calloc(sizeof(char), size);
}

char **createCommands(int n) {
  /* Allocate space for n command pointers */
  return calloc(sizeof(char), n);
}

char *getCommand(size_t size) {
    /* Print the prompt */
    printf("> ");

    /* Allocate memory for a command and set the last char to EOS */
    char *command = createCommand(size);
    command[size-1] = '\0';

    /* Error checking for fgets, returning NULL */
    if (fgets(command, size, stdin) == NULL) {
      printf("Command not understood.\nPlease try again.\n");
      return NULL;
    }

    /* Error checking for readin, if not end of file, fail. */
    if (feof(stdin) != 0) {
      printf("You've entered a command too large!\nPlease try again.\n");
      return NULL;
    }

    return command;
}

char **getCommands(int n, size_t size) {
  char **commands = createCommands(n);
  for (int i = 0; i < n; ++i) {
    while (commands[i] == NULL) { 
      commands[i] = getCommand(size);
    }
  }
  return commands;
}

void printCommands(char **commands, int n) {
  for (int i = 0; i < n; ++i) {
    printf("%s\n", commands[i]);
  }
}

void freeCommands(char **commands, int n) {
  for (int i = 0; i < n; ++i) {
    if (commands[i] != NULL) free(commands[i]);
  }
  free(commands);
}

int main(int argc, char** argv) {
  if (argc > 1) {
    int n = atoi(argv[1]);
    char** commands = getCommands(n, COMMAND_SIZE);
    printCommands(commands, n);
    freeCommands(commands, n);
    return EXIT_SUCCESS;
  }
  return EXIT_FAILURE;
}
