#ifndef MAIN
#define MAIN

#define COMMAND_SIZE 80

char *createCommand(size_t size);
char **createCommands(int n);
char *getCommand(size_t size);
char **getCommands(int n, size_t size);

void printCommands(char **commands, int n);
void freeCommands(char **commands, int n);

#endif
