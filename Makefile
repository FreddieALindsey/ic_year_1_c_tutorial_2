# Set the compiler
CC= gcc

# Set the flags for the C compiler
CFLAGS= -Wall -Werror -g -D_POSIX_SOURCE -pedantic -std=c99

# Build rule for the final executable
main: main.c
	$(CC) $(CFLAGS) main.c -o main.o

# Rule to clean generated files
clean:
	rm -f main.o

# Rule to make all
all: main

# Tell make that ‘clean’ is not a real file
.PHONY: all clean
